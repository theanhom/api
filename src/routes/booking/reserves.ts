import { FastifyInstance, FastifyReply, FastifyRequest } from 'fastify';
import { getReasonPhrase, StatusCodes } from 'http-status-codes';
import { Knex } from 'knex';
import { ReservesModel } from '../../models/booking/reserves';

export default async (fastify: FastifyInstance, _options: any, done: any) => {

  const db: Knex = fastify.db;
  const importModel = new ReservesModel();

  fastify.get('/', async (request: FastifyRequest, reply: FastifyReply) => {

    try {
      let datas :any = await importModel.list(db);

      return reply.status(StatusCodes.CREATED)
        .send({
          status:StatusCodes.CREATED,
          ok: true,
          results : datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.get('/getByID/:id', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id:number = req.params.id;
    try {
      let datas :any = await importModel.getByID(db,id);

      return reply.status(StatusCodes.CREATED)
        .send({
          status:StatusCodes.CREATED,
          ok: true,
          results : datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.get('/getByCustomerID/:id', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id:number = req.params.id;
    try {
      let datas :any = await importModel.getByCustomerID(db,id);

      return reply.status(StatusCodes.CREATED)
        .send({
          status:StatusCodes.CREATED,
          ok: true,
          results : datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })
  fastify.get('/getByServiceTypeID/:id', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id:number = req.params.id;
    try {
      let datas :any = await importModel.getByServiceTypeID(db,id);

      return reply.status(StatusCodes.CREATED)
        .send({
          status:StatusCodes.CREATED,
          ok: true,
          results : datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })
  fastify.get('/getBySlotID/:id', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id:number = req.params.id;
    try {
      let datas :any = await importModel.getBySlotID(db,id);

      return reply.status(StatusCodes.CREATED)
        .send({
          status:StatusCodes.CREATED,
          ok: true,
          results : datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })
  fastify.get('/getByUserID/:id', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id:number = req.params.id;
    try {
      let datas :any = await importModel.getByUserID(db,id);

      return reply.status(StatusCodes.CREATED)
        .send({
          status:StatusCodes.CREATED,
          ok: true,
          results : datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })
  fastify.post('/SearchText', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const data: any = { ...req.body }

    try {
      let datas :any = await importModel.getSearch(db,data.searchtext);

      return reply.status(StatusCodes.CREATED)
        .send({
          status:StatusCodes.CREATED,
          ok: true,
          results : datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.post('/', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const data: any = { ...req.body }

    try {
      let datas :any = await importModel.create(db,data);

      return reply.status(StatusCodes.CREATED)
        .send({
          status:StatusCodes.CREATED,
          ok: true,
          results : datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.put('/:id', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id:number = req.params.id;
    const data: any = { ...req.body }

    try {
      let datas :any = await importModel.update(db,data,id);

      return reply.status(StatusCodes.CREATED)
        .send({
          status:StatusCodes.CREATED,
          ok: true,
          results : datas
        });
    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  fastify.delete('/:id', async (request: FastifyRequest, reply: FastifyReply) => {
    const req: any = request;
    const id:number = req.params.id;
    try {
      let datas :any = await importModel.delete(db,id);
      if(datas){
        return reply.status(StatusCodes.CREATED)
        .send({
          status:StatusCodes.CREATED,
          ok: true,
          results : `Delete ID : ${id} Successfully`
        });
      }else{
        return reply.status(204)
        .send({
          status:204,
          ok: false,
          results : `Delete ID : ${id} Not Success`
        });

      }

    } catch (error: any) {
      request.log.error(error);
      return reply.status(StatusCodes.INTERNAL_SERVER_ERROR)
        .send({
          status: StatusCodes.INTERNAL_SERVER_ERROR,
          ok: false,
          error: getReasonPhrase(StatusCodes.INTERNAL_SERVER_ERROR)
        });
    }
  })

  done();
}