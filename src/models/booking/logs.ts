import { Knex } from 'knex';

export class LogsModel {

  list(db: Knex) {
    return db('logs');
  }  

  getByID(db: Knex, id: number) {
    return db('logs')
    .where('log_id', id);
  }  

  getSearch(db: Knex, text: string) {
    return db('logs')
    .whereLike('log_detail', text);
  }  

  create(db: Knex, data: any) {
    return db('logs')
    .insert(data)
    .returning('*');
  }
  
  update(db: Knex, data: any, id: number) {
    return db('logs')
    .where('log_id', id)
    .update(data)
    .returning('*');
  }
  
  delete(db: Knex, id: number) {
    return db('logs')
      .where('log_id', id)
      .delete();
  }

}