import { Knex } from 'knex';

export class ConfigsModel {

  list(db: Knex) {
    return db('configs');
  }  

  getByID(db: Knex, id: number) {
    return db('configs')
    .where('config_id', id);
  }  

  getSearch(db: Knex, text: string) {
    return db('configs')
    .whereLike('config_name', text);
  }  

  create(db: Knex, data: any) {
    return db('configs')
    .insert(data)
    .returning('*');
  }
  
  update(db: Knex, data: any, id: number) {
    return db('configs')
    .where('config_id', id)
    .update(data)
    .returning('*');
  }
  
  delete(db: Knex, id: number) {
    return db('configs')
      .where('config_id', id)
      .delete();
  }

}